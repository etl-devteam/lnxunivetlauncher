#include <QDirIterator>
#include <QCoreApplication>
#include <QProcess>
#include <QDebug>

#include "appModel.h"


AppModel::AppModel() :
    AbstractVisionARModel(),
    batteryManager{ nullptr },
    numApps       { 0 },
    sliderValue   { 1 },
    status        { MAIN_WINDOW },
    radioOption   { ConfirmSelection::YES }
{
}


AppModel::~AppModel()
{
    if (batteryManager != nullptr)
    {
        delete batteryManager;
        batteryManager = nullptr;
    }
}


void AppModel::blueLED(LEDOption option)
{
    QFile LEDFile(LED_FILE_NAME);

    if (LEDFile.open(QIODevice::WriteOnly| QIODevice::Text))
    {
        LEDFile.write(option == LEDOption::TURN_ON ? "4" : "8");
    }
}


void AppModel::buildApplicationsVector()
{
    QDirIterator homeDinexBinAppsDirIt(APPS_DIRECTORY);
    findApps(homeDinexBinAppsDirIt);

    if (QFile::exists(SD_CARD_DIR))
    {
        QDirIterator sdDirIt("/mnt/mmc/apps");
        findApps(sdDirIt);
    }
}


void AppModel::findApps(QDirIterator &dirIt)
{
    while (dirIt.hasNext())
    {
        QFileInfo nextFileInfo(dirIt.next());

        const QString baseName = nextFileInfo.baseName();

        if (nextFileInfo.isDir() && baseName != "." && baseName != ".." && !baseName.isEmpty())
        {
            const QString absoluteDirPath = nextFileInfo.absoluteFilePath();

            // If an exe with the same name as the parent directory exists
            if (QFile::exists(absoluteDirPath + '/' + baseName))
            {
                if (QFile::exists(absoluteDirPath + '/' + ICON_NAME))
                {
                    appsAndIcons.push_back({absoluteDirPath + '/' + baseName, absoluteDirPath + '/' + ICON_NAME});
                }
                else  // use default icon
                {
                    appsAndIcons.push_back({absoluteDirPath + '/' + baseName, DEFAULT_ICON});
                }
            }
        }
    }
}


void AppModel::initializeLauncher()
{
    numApps = appsAndIcons.size();

    const QString emptyString;

    if (numApps == 0)
    {
        emit displayTitle(emptyString);
        emit displayIcons(emptyString, emptyString, emptyString);
        return;
    }

    emit setSliderMinValue(MIN_SLIDER);

    if (numApps == 1)
    {
        emit displayTitle(appsAndIcons[0].exePath.split("/").last());
        emit displayIcons(emptyString, appsAndIcons[0].iconPath, emptyString);

        sliderValue = 1;
    }
    else if (numApps == 2)
    {
        emit displayTitle(appsAndIcons[1].exePath.split("/").last());
        emit displayIcons(appsAndIcons[0].iconPath, appsAndIcons[1].iconPath, emptyString);
        emit setSliderMaxValue(numApps);

        sliderValue = 2;
    }
    else
    {
        emit displayTitle(appsAndIcons[1].exePath.split("/").last());
        emit displayIcons(appsAndIcons[0].iconPath, appsAndIcons[1].iconPath, appsAndIcons[2].iconPath);
        emit setSliderMaxValue(numApps);

        sliderValue = 2;
    }

    emit setSlider(sliderValue);

    batteryManager = new Battery;
    connect(batteryManager, &Battery::batteryLevel, this, &AppModel::updateBatteryLevel);
    batteryManager->startTimer();
}


void AppModel::launchSelectedApplication() const
{
    QProcess::execute("/bin/sh", {"-c", appsAndIcons[sliderValue - 1].exePath + " &"});
}


void AppModel::runModel()
{
    emit showWindow(FIRST_WINDOW);

    blueLED(LEDOption::TURN_ON);

    buildApplicationsVector();

    initializeLauncher();
}


void AppModel::backReleased(int /*timePressed*/)
{
    if (status == CONFIRM_WINDOW)
    {
        status = MAIN_WINDOW;
        emit showWindow(FIRST_WINDOW);
    }
}


void AppModel::okPressed()
{
    if (status == MAIN_WINDOW)
    {
        status = CONFIRM_WINDOW;

        const QString fullPath = appsAndIcons[sliderValue - 1].exePath;

        // return 'n' rightmost characters, where 'n' is the size of the string minus the position of the last backslash
        emit setExeName(fullPath.right(fullPath.size() - (1 + fullPath.lastIndexOf('/'))));

        emit showWindow(SECOND_WINDOW);
    }
    else
    {
        if (radioOption == ConfirmSelection::YES)
        {
            blueLED(LEDOption::TURN_OFF);

            launchSelectedApplication();

            QCoreApplication::instance()->quit();
        }
        else
        {
            status = MAIN_WINDOW;
            emit showWindow(FIRST_WINDOW);
        }
    }
}


void AppModel::backwardPressed()
{
    if (status == MAIN_WINDOW)
    {
        if (sliderValue > MIN_SLIDER)
        {
            emit setSlider(--sliderValue);

            emit displayTitle(appsAndIcons[sliderValue - 1].exePath.split("/").last());
            emit displayIcons(
                    (sliderValue > MIN_SLIDER ? appsAndIcons[sliderValue - 2].iconPath : ""),
                    appsAndIcons[sliderValue - 1].iconPath,
                    appsAndIcons[sliderValue].iconPath);
        }
    }
    else
    {
        if (radioOption == ConfirmSelection::NO)
        {
            emit toggleRadioButton();
            radioOption = ConfirmSelection::YES;
        }
    }
}


void AppModel::forwardPressed()
{
    if (status == MAIN_WINDOW)
    {
        if (sliderValue < numApps)
        {
            emit setSlider(++sliderValue);

            emit displayTitle(appsAndIcons[sliderValue - 1].exePath.split("/").last());
            emit displayIcons(
                    appsAndIcons[sliderValue - 2].iconPath,
                    appsAndIcons[sliderValue - 1].iconPath,
                    (sliderValue < numApps ? appsAndIcons[sliderValue].iconPath : ""));
        }
    }
    else
    {
        if (radioOption == ConfirmSelection::YES)
        {
            emit toggleRadioButton();
            radioOption = ConfirmSelection::NO;
        }
    }
}


void AppModel::updateBatteryLevel(int percent)
{
    emit batteryLevelUpdate(percent);
}
