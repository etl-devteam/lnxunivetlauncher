#ifndef BATTERYMANAGER_H
#define BATTERYMANAGER_H

#include <QObject>


QT_FORWARD_DECLARE_CLASS(QTimer)


class Battery final : public QObject
{
    Q_OBJECT

public:
    Battery();
    ~Battery();

    void startTimer();
    void stopTimer();

private:
    static constexpr char const * const BATTERY_FILE_NAME{ "/sys/devices/soc0/DinemaPowerBank/PercentBattery" };
// DEBUG    static constexpr char const * const BATTERY_FILE_NAME{ "/home/dinex/PercentBattery" };

    static constexpr unsigned TIMER_MILLISECONDS{ 15000 };

    QScopedPointer<QTimer> timer;

signals:
    void batteryLevel(int percent);

private slots:
    void checkBatteryLevel();
};


#endif // BATTERYMANAGER_H
