#ifndef APPMODEL_H
#define APPMODEL_H

#include <VisionAR/Framework/abstractVisionARModel.h>

#include "battery.h"


QT_FORWARD_DECLARE_CLASS(QDirIterator)


class AppModel final : public AbstractVisionARModel
{
    Q_OBJECT

public:
    AppModel();
    ~AppModel();

private:

    enum : short {
        FIRST_WINDOW = 1, SECOND_WINDOW,
    };

    enum AppStatus : short {
        MAIN_WINDOW, CONFIRM_WINDOW,
    };

    enum class LEDOption : short {
        TURN_ON, TURN_OFF
    };

    enum class ConfirmSelection : short {
        YES, NO
    };

    static constexpr char const * const LED_FILE_NAME { "/sys/class/dinema/led_RGB"        };
    static constexpr char const * const APPS_DIRECTORY{ "/home/dinex/bin/apps"             };
    static constexpr char const * const DEFAULT_ICON  { ":/img_launcher/icona_default.png" };
    static constexpr char const * const ICON_NAME     { "icon.png"                         };
    static constexpr char const * const SD_CARD_DIR   { "/dev/mmcblk1p1"                   };

    static constexpr unsigned MIN_SLIDER{ 1 };

    Battery *batteryManager;

    unsigned numApps;
    unsigned sliderValue;

    AppStatus status;
    ConfirmSelection radioOption;

    // This struct simply links together the exe name and icon paths
    struct PathExeIcons
    {
        QString exePath;
        QString iconPath;
    };
    QVector<PathExeIcons> appsAndIcons;

    void runModel()        override;
    void backPressed()     override {}
    void okPressed()       override;
    void forwardPressed()  override;
    void backwardPressed() override;

    void doubleTap()                                     override {}
    void swipeForward()                                  override {}
    void swipeBackward()                                 override {}
    void backReleased(    int   timePressed  )           override;
    void okReleased(      int /*timePressed*/ = 0)       override {}
    void forwardReleased( int /*timePressed*/)           override {}
    void backwardReleased(int /*timePressed*/)           override {}
    void singleTap()                                     override {}
    void manageAlertsEnd(QString /*id*/, bool /*value*/) override {}

    static void blueLED(LEDOption option);

    void buildApplicationsVector();
    void initializeLauncher();

    void launchSelectedApplication() const;

    void findApps(QDirIterator &dirIt);

private slots:
    void updateBatteryLevel(int percent);

signals:
    void batteryLevelUpdate(int percent);

    void setExeName(const QString &name);

    void displayTitle(const QString &title);
    void displayIcons(const QString &left, const QString &centre, const QString &right);

    void setSliderMaxValue(int value);
    void setSliderMinValue(int value);
    void setSlider(int value);

    void toggleRadioButton();
};


#endif // APPMODEL_H
