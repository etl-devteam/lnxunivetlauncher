#include "mainWindow.h"


MainWindow::MainWindow() :
    mainWindow{ new Ui_MainWindow }
{
}


MainWindow::~MainWindow()
{
    delete mainWindow;
    mainWindow = nullptr;
}


void MainWindow::setupUi(QWidget *widget)
{
    mainWindow->setupUi(widget);
}


void MainWindow::setSliderMinValue(int value)
{
    mainWindow->horizontalSlider->setMinimum(value);
}


void MainWindow::setSliderMaxValue(int value)
{
    mainWindow->horizontalSlider->setMaximum(value);
}


void MainWindow::setSlider(int value)
{
    mainWindow->horizontalSlider->setValue(value);
}


void MainWindow::updateBatteryLevel(int batteryPercent)
{
    if (batteryPercent < 25)
    {
        mainWindow->batteryImg->setPixmap(QPixmap(":/img_launcher/low_battery.png").scaled(mainWindow->batteryImg->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }
    else if (batteryPercent >= 25 && batteryPercent < 50)
    {
        mainWindow->batteryImg->setPixmap(QPixmap(":/img_launcher/med_low_battery.png").scaled(mainWindow->batteryImg->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }
    else if (batteryPercent >= 50 && batteryPercent < 75)
    {
        mainWindow->batteryImg->setPixmap(QPixmap(":/img_launcher/med_high_battery.png").scaled(mainWindow->batteryImg->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }
    else
    {
        mainWindow->batteryImg->setPixmap(QPixmap(":/img_launcher/high_battery.png").scaled(mainWindow->batteryImg->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }

    mainWindow->batteryImg->show();
}


void MainWindow::displayTitle(const QString &title)
{
    if (title.isEmpty())
    {
        mainWindow->centreAppName->hide();
    }
    else
    {
        mainWindow->centreAppName->setText(title);
        mainWindow->centreAppName->show();
    }
}


void MainWindow::displayIcons(const QString &left, const QString &centre, const QString &right)
{
    if (left.isEmpty())
    {
        mainWindow->leftImg->hide();
    }
    else
    {
        mainWindow->leftImg->setPixmap(QPixmap(left).scaled(mainWindow->leftImg->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
        mainWindow->leftImg->show();
    }

    if (centre.isEmpty())
    {
        mainWindow->centreImg->hide();
        mainWindow->noAppsLabel->show();
    }
    else
    {
        mainWindow->noAppsLabel->hide();
        mainWindow->centreImg->setPixmap(QPixmap(centre).scaled(mainWindow->centreImg->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
        mainWindow->centreImg->show();
    }

    if (right.isEmpty())
    {
        mainWindow->rightImg->hide();
    }
    else
    {
        mainWindow->rightImg->setPixmap(QPixmap(right).scaled(mainWindow->rightImg->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
        mainWindow->rightImg->show();
    }
}
