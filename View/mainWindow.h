#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_main_window.h"


class MainWindow final
{
public:
    MainWindow();
    ~MainWindow();

    void setupUi(QWidget *widget);

    void updateBatteryLevel(int percent);

    void displayTitle(const QString &title);
    void displayIcons(const QString &left, const QString &centre, const QString &right);

    void setSliderMinValue(int value);
    void setSliderMaxValue(int value);
    void setSlider        (int value);

private:
    Ui_MainWindow *mainWindow;
};


#endif // MAINWINDOW_H
