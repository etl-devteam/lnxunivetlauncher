#ifndef APPVIEW_H
#define APPVIEW_H

#include <VisionAR/Framework/abstractVisionARView.h>

#include "mainWindow.h"
#include "confirmWindow.h"


class AppView final : public AbstractVisionARView
{
    Q_OBJECT

public:
    AppView();

private:
    QScopedPointer<MainWindow>    mainWindow;
    QScopedPointer<ConfirmWindow> confirmWindow;

public slots:
    void batteryLevelUpdate(int percent);

    void setExeName(const QString &name);

    void displayTitle(const QString &title);
    void displayIcons(const QString &left, const QString &centre, const QString &right);

    void setSliderMaxValue(int value);
    void setSliderMinValue(int value);
    void setSlider        (int value);

    void toggleRadioButton();
};


#endif // APPVIEW_H
