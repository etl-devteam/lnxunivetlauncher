#include "appView.h"


AppView::AppView() :
    AbstractVisionARView(),
    mainWindow   { new MainWindow },
    confirmWindow{ new ConfirmWindow }
{
    // the order in which you "register window"s here
    // matches the number you pass in "showWindow(int)"

    registerWindow(mainWindow.data());
    registerWindow(confirmWindow.data());
}


void AppView::batteryLevelUpdate(int percent)
{
    mainWindow->updateBatteryLevel(percent);
}


void AppView::displayTitle(const QString &title)
{
    mainWindow->displayTitle(title);
}


void AppView::displayIcons(const QString &left, const QString &centre, const QString &right)
{
    mainWindow->displayIcons(left, centre, right);
}


void AppView::setSliderMaxValue(int value)
{
    mainWindow->setSliderMaxValue(value);
}


void AppView::setSliderMinValue(int value)
{
    mainWindow->setSliderMinValue(value);
}


void AppView::setSlider(int value)
{
    mainWindow->setSlider(value);
}


void AppView::toggleRadioButton()
{
    confirmWindow->toggleRadioButton();
}


void AppView::setExeName(const QString &name)
{
    confirmWindow->setExeName(name);
}
