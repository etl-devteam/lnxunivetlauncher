#ifndef CONFIRMWINDOW_H
#define CONFIRMWINDOW_H

#include "ui_confirm_window.h"


class ConfirmWindow final
{
public:
    ConfirmWindow();
    ~ConfirmWindow();

    void setupUi(QWidget *widget);

    void toggleRadioButton();
    void setExeName(const QString &name);

private:
    Ui_ConfirmWindow *confirmWindow;
};


#endif // CONFIRMWINDOW_H
