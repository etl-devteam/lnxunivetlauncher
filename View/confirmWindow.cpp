#include "confirmWindow.h"


ConfirmWindow::ConfirmWindow() :
    confirmWindow{ new Ui_ConfirmWindow }
{
}


ConfirmWindow::~ConfirmWindow()
{
    delete confirmWindow;
    confirmWindow = nullptr;
}


void ConfirmWindow::setupUi(QWidget *widget)
{
    confirmWindow->setupUi(widget);
}


void ConfirmWindow::toggleRadioButton()
{
    if (confirmWindow->radioNo->isChecked())
    {
        confirmWindow->radioYes->toggle();
    }
    else if (confirmWindow->radioYes->isChecked())
    {
        confirmWindow->radioNo->toggle();
    }
}


void ConfirmWindow::setExeName(const QString &name)
{
    confirmWindow->appNameLabel->setText(name);
}
