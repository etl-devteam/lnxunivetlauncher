#include "appController.h"


AppController::AppController(AppView *view, AppModel *model) :
    AbstractVisionARController(view, model)
{
}


void AppController::connections()
{
    connect((AppModel*)model, SIGNAL(toggleRadioButton()),         (AppView*)view, SLOT(toggleRadioButton()));
    connect((AppModel*)model, SIGNAL(batteryLevelUpdate(int)),     (AppView*)view, SLOT(batteryLevelUpdate(int)));
    connect((AppModel*)model, SIGNAL(setExeName(const QString &)), (AppView*)view, SLOT(setExeName(const QString &)));

    connect((AppModel*)model, SIGNAL(displayTitle(const QString &)),(AppView*)view, SLOT(displayTitle(const QString &)));
    connect((AppModel*)model, SIGNAL(displayIcons(const QString &, const QString &, const QString &)),
            (AppView*) view,  SLOT(  displayIcons(const QString &, const QString &, const QString &)));

    connect((AppModel*)model, SIGNAL(setSliderMinValue(int)), (AppView*)view, SLOT(setSliderMinValue(int)));
    connect((AppModel*)model, SIGNAL(setSliderMaxValue(int)), (AppView*)view, SLOT(setSliderMaxValue(int)));
    connect((AppModel*)model, SIGNAL(setSlider        (int)), (AppView*)view, SLOT(setSlider        (int)));
}


void AppController::endActions()
{
}
